package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

public class GeneratedFileDto {

    private InputStreamResource resource;
    private Long contentLength;
    private String fileNameWithExtension;

    public GeneratedFileDto(Builder builder) {
        resource = builder.resource;
        contentLength = builder.contentLength;
        fileNameWithExtension = builder.fileNameWithExtension;
    }

    public InputStreamResource getResource() {
        return resource;
    }

    public Long getContentLength() {
        return contentLength;
    }

    public String getFileNameWithExtension() {
        return fileNameWithExtension;
    }

    public static final class Builder {
        private InputStreamResource resource;
        private Long contentLength;
        private String fileNameWithExtension;

        public Builder() {
        }

        public Builder resource(InputStreamResource resource) {
            this.resource = resource;
            return this;
        }

        public Builder contentLength(Long contentLength) {
            this.contentLength = contentLength;
            return this;
        }

        public Builder fileNameWithExtension(String fileNameWithExtension) {
            this.fileNameWithExtension = fileNameWithExtension;
            return this;
        }

        public GeneratedFileDto build() {
            return new GeneratedFileDto(this);
        }
    }
}
