package pl.edu.pwsztar.domain.files;

import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.GeneratedFileDto;

import java.io.IOException;


public interface TextFileGenerator {

    GeneratedFileDto toTxt(FileDto fileDto) throws IOException;
}
